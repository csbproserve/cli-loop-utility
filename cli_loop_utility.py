from datetime import datetime
from time import sleep
import rich_click as click
from scrapli.driver.core import IOSXEDriver
from rich import print

def output_logger(log_message, file_output, log_prefix, datetime_suffix):
    if file_output:
        log_filename = f"{log_prefix}-{datetime_suffix}.log"
        with open(log_filename, mode="a") as log:
            log.write(log_message)
    print(log_message)

@click.group()
def cli():
    """A tool for collecting CLI command data"""
    pass

@click.command("command-logger")
@click.option("--device", required=True, help="device ip address or hostname")
@click.option("--username", required=True, help="username to use to login to device")
@click.password_option()
@click.option("--file-output/--no-file-output", default=False)
@click.option("--log-prefix", required=False, help="prefix for generated log files")
@click.option("--interval", required=True, type=int, help="interval at which to run selected commands in seconds")
@click.option("--commands", required=True, help="filename to load commands from, one command per line")
def command_logger(device, username, password, file_output, log_prefix, interval, commands):
    device = {
        "host": device,
        "auth_username": username,
        "auth_password": password,
        "auth_strict_key": False,
    }
    with open(commands) as f:
        command_list = f.readlines()

    conn = IOSXEDriver(**device)
    conn.open()
    while (True):
        datetime_suffix = datetime.now().strftime("%Y.%m.%d-%H.%M.%S")
        for command in command_list:
            response = conn.send_command(command)
            output_logger(log_message=response.result, file_output=file_output, log_prefix=log_prefix, datetime_suffix=datetime_suffix)
        print(f"---Sleeping {interval} seconds---")
        sleep(interval)

cli.add_command(command_logger)        

if __name__ == "__main__":
    cli()